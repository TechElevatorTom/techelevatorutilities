# TechElevatorUtilities
Utilities for moving daily exercises, creating pair repositories, etc.

## Environment Setup

1. Python 3.7 must be available on your path
2. You'll need [pipenv](https://pipenv.readthedocs.io/en/latest/) package installed and the pipenv command must be available on your path. Pipenv can be install with `pip install pipenv` if `pip` is available on the command line
3. run `pipenv install`
4. run `pipenv shell` to get a python shell


## Package Setup

1. Copy config.json.example into config.json
2. Replace values in config.json with your configuration


## Running the Program

First complete the environment and package setup. Then you can run the command line program by issuing the command

    python run.py
    
The application has built-in help describing the various commands and options. It's structured as run.py <command> <subcommand> similar to 

    git pull origin main
    
For your convenience scripts have been provided to invoke the python, which assume you're already in a pipenv shell. 

For windows the script command is `.\te.cmd` for linux/bash/mac the command is `./te`


## Workflows

Note that for all the following commands the `--language` parameter is optional. The program will either use the value configured in yuour `config.json`, or `java` if neither value is supplied

### Initial Setup

This assumes you've completed the steps in *package setup* above and you have a properly labeled config file. This also assumes your config file has the correct team identifier and bitbucket app password setup. The following steps would need to be completed once, per cohort, per language platform.


You'll need a CSV file matching the example file in the repo. Import the students into the program by running the following command. (note, the students will be stored in `~/.techelevator/<language>-students.json`  if manual corrections are needed.

    ./te --language java state load-students --csv-file java-students.csv
    
To create repos for the students in bitbucket you can run the following command (assuming the students have been loaded already)

    ./te --language c# repos provision individual-repos

Create your local repository with branches mapped per student with the following command

    ./te --language c# repos students initialize-local

This will create a repository in your `student-workspace` directory configured in the `config.json` formatted as `<language>-repository` with branches configured for each student.

### Day to Day

**Coming soon** 
The program will have a command to deploy a day's worth of assignments into the main branch of the student repo. For now, manually switch to the main branch of the `<language>-repository` git folder, switch to the `main` branch and manually copy in the relevant information and commit it to main

### Fetch Student changes

    ./te --language c# repos students fetch
    
### Deploy changes to student branches

First ensure the main branch of the `<language>-repository` has the commit you'd like to send out for te day. Then run the following command

    ./te --language c# repos students send-commit

This will fetch all the student commits from their relevant remote repositories switch to their branch and merge in these changes from main. if the option `p` or `--push` flag is supplied the change will then be pushed to the student's repository. The suggested workflow is to run the command without the push to ensure all merges are clean, manually resolve any conflict, then rerun the command with the `-p` flag to deploy the changes to the students remotes.



## **Pull requests are welcome**


