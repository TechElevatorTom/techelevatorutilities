import techelevator
from techelevator.configuration import Configuration
from techelevator.student_repos import StudentRepos
from techelevator.curriculum_folder import Curriculum
from techelevator.state_management import StateManager


global_config = None

def get_config():
    global global_config
    if global_config is None:
        global_config = Configuration()
        global_config.load()
    return global_config

def get_student_repo(config = None):
    if config is None:
        config = get_config()
    repos = StudentRepos(config.studentConfig())
    return repos

def get_students():
    mgr = StateManager(get_config())
    return mgr.read_students()

def get_curriculum():
    config = get_config()
    curriculum = Curriculum(config.curriculum_folder)
    return curriculum
    
import click


@click.group()
@click.option('--language',
              type=click.Choice(['java','c#']),
              help='Language, defaults to java. Can be configured in your configuration file',
              default=None)
@click.option('--class-name',
              type=click.STRING,
              help='class name, such as "blue" or "1" for campuses with multiple classes per language',
              default=None)
def cli(language,class_name):
    """Manage your tech elevator student environment. Make sure you have a config.json setup in the directory where you run this tool"""

    global_config = get_config()
    if language is not None:
        global_config.language = language
    if class_name is not None:
        global_config.class_name = class_name

    print("configured language: ",get_config().language)
    print("configured class name: ",get_config().class_name)
    print("student config ", get_config().studentConfig().language)
    pass


@cli.group()
def state():
    """Manage the local application state. Some examples areo adding/removing students, etc"""
    pass

@state.command()
@click.option('--csv-file',
              prompt=True,
              type=str,
              help='Path to a csv file of students to load')
def load_students(csv_file):
    """Import new group of students from CSV. WARNING replaces existing students for a given language

WARNING: This will replace currently loaded students for the provided language
 """    
    config = get_config()
    manager = StateManager(config)
    manager.import_students(csv_file)
    pass

@state.command()
def read_students():
    """Displays all the students in our collection"""
    students = get_students().students
    for student in students:
        print(student.name)

    if len(students) == 0:
        print("No students present, use `load-students` to import new students")


@cli.group()
def repos():
    #print("",kwargs)
    pass

@repos.group()
def curriculum():
    """Manage the curriculum repository"""
    pass

@curriculum.command(name='fetch')
@click.option('--reset-hard',
        is_flag=True,
        help='Perform a hard reset on the curriculum folder before pulling. defaults to creating a stash')
def fetch_curriculum(**kwargs):
    """Update the curriculum repository, stashing any changes by default"""
    cur = get_curriculum()
    cur.pull_latest(kwargs['reset_hard'])
    pass


@repos.group()
def students():
    """Manage the student repositories, individual exercises and pairs"""
    pass


@students.command()
def initialize_local():
    """Setup the working directory with branches for each student""" 
    config = get_config()
    manager = StateManager(config)
    repo = get_student_repo(config)
    repo.initialize_exercise_repo(manager.read_students())

@students.command(name='fetch')
@click.option('--stash-changes',is_flag=True)
def fetch_student(**kwargs):
    """Update the student repositories from bitbucket"""    
    stash = kwargs['stash_changes']
    print("Stash changes",stash)
    config = get_config()
    manager = StateManager(config)
    repo = get_student_repo(config)
    print("Fetching all student repos from bitbucket")
    repo.collect_student_updates(manager.read_students(),stash)


@students.command()
@click.option('--push',
              '-p',
              is_flag=True,
              help='Push to student repos after merging')
@click.option('--stash',
              '-s',
              is_flag=True,
              help='stash current branch of the repo before performing operation'
              )
def send_commit(**kwargs):
    ''' Merge a commit from main into each student branch, optionally deploy'''
    students = get_students()
    s_repos = get_student_repo()

    s_repos.commit_to_student_branches(students,
                                       kwargs['push'],
                                       kwargs['stash'])

    

@repos.group()
def provision():
    '''Provision repositories'''
    pass
    
@provision.command()
def individual_repos():
    """Create bitbucket repositories for all students"""
    config = get_config()
    repo = get_student_repo(config)
    manager = StateManager(config)
    repo.create_student_repos(manager.read_students())

@provision.command()
@click.option('--number-teams',
              prompt=True,
              type=int,
              help='Number of teams to create')
@click.option('--week-number',
              prompt=True,
              type=int,
              help='Week of pair work')
def pair(**kwargs):
    '''Create a set of repositories for pair exercises'''
    teams = kwargs['number_teams']
    week = kwargs['week_number']
    s_repos = get_student_repo()
    s_repos.create_pair_repos(week,teams)
    click.echo("Created repos for week {}".format(str(week)))
    
@provision.command()
@click.option('--number-teams',
              prompt=True,
              type=int,
              help='Number of teams to create')
@click.option('--module-number',
              type=int,
              prompt=True,
              help='Module for the capstone repo')
def capstone(**kwargs):
    """Setup repos for module capstones"""
    teams = kwargs['number_teams']
    module = kwargs['module_number']
    click.echo("{},{}".format(str(teams),str(module)))
    s_repos = get_student_repo()
    s_repos.create_capstone_repos(module,teams)
    pass
    

@provision.command()
@click.option('--team-name',
              prompt=True,
              help='Name of the capstone team')
def final_capstone(**kwargs):
    '''Create a repository by name for final capstones'''
    team_name = kwargs['team_name']
    s_repos = get_student_repo()
    s_repos.create_final_capstone(team_name)

@provision.command()
def project():
    '''Create bitbucket project for your current language'''
    s_repos = get_student_repo()
    s_repos.create_project()
    
@cli.command()
def assignments():
    click.echo()
    pass

if __name__ == '__main__':
    cli()
