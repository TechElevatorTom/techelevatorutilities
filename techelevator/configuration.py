import json
import os
from typing import List, Optional
from .student_info import StudentRepoConfig

class Configuration:
    
    def __init__(self):
        self._config_json = None
        self._repo_config = None
        self.workspace = None
        self.curriculum_workspace = None
        self.team = None
        self._language = 'java'
        self._class_name = None
        

    @property
    def class_name(self):
        return self._class_name

    @class_name.setter
    def class_name(self,class_name):
        self._class_name = class_name
        if(self._repo_config != None):
            self._repo_config.class_name = class_name
        
    @property
    def language(self):
        return self._language

    @language.setter
    def language(self,lang):
        self._language = lang
        if(self._repo_config != None):
            self._repo_config.language = lang

    def studentConfig(self):        
        if self._config_json == None:
            self.load()

        if self._repo_config != None:
            return self._repo_config
            
        js = self._config_json
        bitbucket = js['bitbucket']
        clone_type = 'ssh' if 'useSsh' in bitbucket and bitbucket['useSsh'] else 'https'
        self._repo_config = StudentRepoConfig(clone_type,
                                             bitbucket['username'],
                                             bitbucket['apppassword'],
                                              bitbucket['team'],
                                              js['studentFolder'])
        self._repo_config.language = self.language
        self._repo_config.class_name = self.class_name
        return self._repo_config 
        
        

        
    def load(self,filename = "./config.json"):
        with open(filename) as fl:
            self._config_json = json.load(fl)

        js = self._config_json

        self.workspace =  js['workspaceFolder']
        self.workspace = os.path.abspath(self.workspace)

        self.curriculum_folder = js['curriculumFolder']
        self.curriculum_folder = os.path.abspath(self.curriculum_folder)
        if 'defaultLanguage' in js:
            self.language = js['defaultLanguage']
        if 'defaultClassName' in js:
            self.class_name = js['defaultClassName']
            
        self.studentConfig()




