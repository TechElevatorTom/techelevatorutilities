import os
from os import path
import re
import git
from git import Repo

class Curriculum:
    
    default_curriculum = "~/workspace-curriculum/te-curriculum-v1"
    curriculum_root = ""
    _day_regex = re.compile(r"[0-9][0-9]_")
    def __init__(self, curriculum_path):
        self.curriculum_root = path.abspath(curriculum_path)

    def get_lessons(self, module):
        module_path = path.join(self.curriculum_root, "module-"+str(module))

        return [x.name for x in os.scandir(module_path)
                if x.is_dir() and self._day_regex.match(x.name) ]
                
    def pull_latest(self,reset_hard = False):
        r = Repo(self.curriculum_root)
        if(reset_hard):
            r.git.reset('--hard')
        else:
            r.git.stash('save')
        o = r.remotes.origin
        o.pull()
