from dataclasses import field
import marshmallow.validate
from typing import List, Optional
from marshmallow import Schema, fields, pprint
import os
import csv
from .student_info import StudentInfo, StudentCollection
from .configuration import Configuration

        

class StateManager:
    config : Configuration
    
    def __init__(self, cfg):
        self.config = cfg
        self.tempdir = os.path.expanduser("~/.techelevator")
        
        if self.config.class_name is None:
            self.student_filename = "{}-students.json".format(self.config.language)
        else:
            self.student_filename = "{}-{}-students.json".format(self.config.language,self.config.class_name)
            
        self.student_filename = os.path.join(self.tempdir,
                                             self.student_filename)
        
        if not os.path.exists(self.tempdir):
            os.makedirs(self.tempdir)
        
        
    def import_students(self,csv_file):
        students = []
        with open(csv_file,'r') as fl:
            reader = csv.reader(fl,delimiter=',')
            for row in reader:
                pprint(row)
                info = StudentInfo(row[0].strip(),row[1].strip())
                students.append(info)

        students = StudentCollection(students)
        serializer = StudentCollection.Schema()
        marshalled = serializer.dumps(students)
        with open(self.student_filename,'w') as fl:
            fl.write(marshalled.data)


    def read_students(self):
        if not os.path.exists(self.student_filename):
            return StudentCollection([])
        
        with open(self.student_filename,'r') as fl:
            json = fl.read()
            unmarshalled = StudentCollection.Schema().loads(json)
            return unmarshalled.data
        
