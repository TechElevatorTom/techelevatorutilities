from dataclasses import field
from marshmallow_dataclass import dataclass # Importing from marshmallow_dataclass instead of dataclasses
from typing import List, Optional


class StudentRepoConfig:
    def __init__(self,clone_type = 'https'
                 ,username = ''
                 ,app_password = ''
                 ,team=''
                 ,workspace_directory = ''):
        self.auth_tuple = (username,app_password)
        self.clone_type = clone_type
        self.bitbucket_team = team
        self.workspace = workspace_directory
        self.api_url_repo = 'https://api.bitbucket.org/2.0/repositories/{}/'.format(team)
        self.api_url_project = 'https://api.bitbucket.org/2.0/teams/{}/projects/'.format(team)
        self.http_url = 'https://bitbucket.org/{}/'.format(team)

@dataclass
class StudentInfo:
    name : str
    bitbucket_user : str
    
@dataclass
class StudentCollection:
    students : List[StudentInfo]
