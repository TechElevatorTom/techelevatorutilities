import requests
import re
import os
from git import Repo
import glob
import json
import shutil
import stat
from pathlib import Path

from .student_info import StudentInfo, StudentCollection, StudentRepoConfig
        
    
class BitbucketRepo:
    def __init__(self,bitbucket_dict,clone_type):
        self.student_name = ''
        self.name = bitbucket_dict['name']
        url_list = bitbucket_dict['links']['clone']
        self.url = ''
        for entry in url_list:
            if(entry['name'] == clone_type):
                self.url = entry['href']

    def __print__(self):
        return "{} {}".format(self.name,self.url)

    def __str__(self):
        return self.__print__()

    def __repr__(self):
        return self.__print__()

    def __unicode__(self):
        return self.__print__().encode('utf8')
    


        
class StudentRepos:
    _exercises_re = re.compile('[A-z-]+?-exercises')
    
    def __init__(self, config):
        self._config = config
        self._exercise_repos = []
    

    def _name_in_repo(repo : BitbucketRepo, name_list):
        repo_name = repo.name.lower()
        for name in name_list:
            if name.lower() in repo_name:
                repo.student_name = name
                return True
        return False


    def _project_key_from_language(language : str):
        projects = {
            "c#" : "NET",
            "java" : "JAVA"
        }

        return projects.get(language,'JAVA')
        
        
    # Convert the bitbucket repos into real objects and filter them
    def _filter_exercise_repos(dict_list,clone_type,name_list,language,class_name):
        repo_list = [BitbucketRepo(x,clone_type)
                     for x in dict_list]

        repo_list = [x for x in repo_list if language in x.name.lower()]
        if class_name is not None:
            #filter the list only to this class
            class_name = '-'+class_name.lower()
            repo_list = [x for x in repo_list if class_name in x.name.lower()]
        
        return [x for
                x in repo_list
                if StudentRepos._name_in_repo(x,name_list)]
        
    
    # fetch all the repos from the bitbucket url and convert them to our
    # internal representation
    def collect_exercise_repos(self, names):
        self._exercise_repos.clear()
        result = requests.get(self._config.api_url_repo,auth = self._config.auth_tuple)
        rdict = result.json()
        class_name = self._config.class_name
        language = self._config.language.lower()
        self._exercise_repos.extend(
            StudentRepos._filter_exercise_repos(rdict['values'],
                                                self._config.clone_type,
                                                names,
                                                language,
                                                class_name))
            
        while(len(rdict['values']) > 0 and 'next' in rdict):
            result = requests.get(rdict['next'],auth=self._config.auth_tuple)
            rdict = result.json()
            self._exercise_repos.extend(
                StudentRepos._filter_exercise_repos(rdict['values'],
                                                    self._config.clone_type,
                                                    names,
                                                    language,
                                                    class_name
                                                    ))          


    def _remove_readonly_(func,path,exc):
        # Check if file access issue
        if not os.access(path, os.W_OK):            
       # Try to change the permision of file
            os.chmod(path, stat.S_IWUSR)
       # call the calling function again
        func(path)
        

    def _get_git_names(students: StudentCollection):
        names = [x.name.replace(' ','').lower() for x in students.students]
        names.append('techelevator-instructors')
        return names


    def _set_repo_path_(self):
        path = str(Path(self._config.workspace).expanduser().resolve())

        if not os.path.exists(path):
            os.mkdir(path)
        language = self._config.language
        class_name = self._config.class_name
        
        pathstr = ""
        if class_name is None:
            pathstr = "{}-repository".format(language)
        else:
            pathstr = "{}-{}-repository".format(language,class_name)
                
        path = os.path.join(path,pathstr)
        self._repo_path = path
        return path
        
    
    def initialize_exercise_repo(self,students : StudentCollection):
        names = StudentRepos._get_git_names(students)
        path = self._set_repo_path_()

        
        # if this repo exists already we'll destroy it and recreate
        if os.path.exists(str(path)): 
            if os.path.isdir(str(path)):
                shutil.rmtree(str(path),
                              ignore_errors=False,
                              onerror=StudentRepos._remove_readonly_)
            else:
                raise Exception("Exercise repo exists as file {}".format(path))

        self.collect_exercise_repos(names)

        repo = Repo.init(str(path))

        with open(os.path.join(str(path),'README.md'),'w') as fl:
            fl.write('# Welcome to TechElevator')

        git = repo.git
        index = repo.index
        git.add('.')
        index.commit('Intial Commit')


        for bb in self._exercise_repos:
            repo.create_remote(bb.student_name,bb.url)
            repo.create_head(bb.student_name)


    def collect_student_updates(self,
                                students : StudentCollection,
                                stash_changes = False):
        names = StudentRepos._get_git_names(students)
        path = self._set_repo_path_()

        if not os.path.exists(path):
            return False
        
        repo = Repo(path)
        git = repo.git

        # save the existing state or reset
        if stash_changes:
            git.stash('save')
        else:
            git.reset('--hard')
            
        for name in names:
            print('Switching to {} and pulling latest changes'.format(name),
                  flush=True)
            git.checkout('main')
            git.checkout(name)
            remote = repo.remote(name)
            try:
                remote.pull('main',allow_unrelated_histories=True)
            except:
                print('Unable to pull changes for {}'.format(name),flush=True)
                               


    def commit_to_student_branches(self,
                                    students: StudentCollection,
                                    push_changes = False,
                                    stash_changes = False):

        self.collect_student_updates(students,stash_changes)
        
        names = StudentRepos._get_git_names(students)
        path = Path(self._set_repo_path_())

        if not path.exists():
            return false

        repo = Repo(str(path))
        git = repo.git

        if stash_changes:
            git.stash('save')
        else:
            git.reset('--hard')

        for name in names:
            git.checkout('main')
            print('Switching to {} and merging latest changes from main'.format(name),
                  flush=True)
            git.checkout(name)
            git.merge('main', '--strategy-option=ours',allow_unrelated_histories=True)
            if push_changes:
                print('Pushing changes to ',name, flush=True)
                remote = repo.remotes[name]
                remote.push('{}:main'.format(name))

                        
            
    #Given the the repos, convert them to an http link
    def repo_http_links(self):
        url_template = self._config.http_url
        url_template += '{0}/src/main/'
        return [url_template.format(x.name) for x in self._exercise_repos]


    def clone_or_update(self,stash_changes = False):
        path = self._config.workspace
        if not os.path.exists(path):
            os.mkdir(path)

        for student_repo in self._exercise_repos:
            repo_name = re.search(r"/([^/]+?).git",student_repo.url).group(1)
            repo_path = os.path.join(path,repo_name)
           
            if not os.path.exists(repo_path):
                print("About to create repo:", repo_name,flush=True)
                Repo.clone_from(student_repo.url, repo_path)

            else:
                print("Found repo for:", repo_name,flush=True)
                repo = Repo(repo_path)
                origin = repo.remotes.origin

                if stash_changes:
                    repo.git.stash('save')
                else:
                    repo.git.reset('--hard')

                origin.pull('main')


    def create_student_repos(self,
                             students: StudentCollection):
        cfg = self._config
        names = StudentRepos._get_git_names(students)
        language = cfg.language
        class_name = cfg.class_name
        
        for name in names:
            repo_url = ""
            repo_name = ""
            if class_name is None:
                repo_name = "{}-{}".format(name,language.lower())
                repo_url = "{}{}-{}".format(cfg.api_url_repo,name,language)
            else:
                repo_name = "{}-{}-{}".format(name,language.lower(),class_name.lower())
                repo_url = "{}{}-{}-{}".format(cfg.api_url_repo,name,language,class_name)

            repo = {
                "name" : repo_name,
                "is_private" : True,                
                "scm" : "git",
                "language" : language,
                "project" : {"key" : StudentRepos._project_key_from_language(language.lower())}
            }
            print('Provisioning repo for: ', name, flush=True)
            result = requests.post(repo_url,json=repo,auth=cfg.auth_tuple)
                
    def create_capstone_repos(self,module_number,number_repos):
        cfg = self._config

        for i in range(0,number_repos):
            name = "{} Module {} Capstone Team {}".format(
                cfg.language.capitalize(),
                str(module_number),
                str(i))

            repo_url = "{}{}-module-{}-capstone-team-{}".format(
                cfg.api_url_repo,
                cfg.language,
                str(module_number),
                str(i))

            repo = {
                "name" : name,
                "is_private":True,
                "scm":"git",
                "language" : cfg.language,
                "project":{"key":StudentRepos._project_key_from_language(cfg.language)}}

            result = requests.post(repo_url,json=repo,auth=cfg.auth_tuple)
            

    def create_project(self):
        cfg = self._config
        project_key = StudentRepos._project_key_from_language(cfg.language)

        project = {
            'is_private':True
            }

        if project_key == 'NET':
            project['name'] = '.NET'
        elif project_key == 'JAVA':
            project['name'] = 'Java'
            
        project['key'] = project_key
        result = requests.post(cfg.api_url_project,json=project,auth=cfg.auth_tuple)



            
    def create_pair_repos(self,week,number_repos):
        
        cfg = self._config
            
        for i in range(0,number_repos):

            name = "{} Week {} Pair Exercises Team {}".format(
                cfg.language.capitalize(),
                str(week),
                str(i))
            
            repo_url = "{}{}-week-{}-pair-exercises-team-{}".format(
                cfg.api_url_repo,
                cfg.language,
                str(week),
                str(i))
            
            repo = {
                "name" : name,
                "is_private":True,
                "scm":"git",
                "language" : cfg.language,
                "project":{"key":StudentRepos._project_key_from_language(cfg.language.lower())}
                    }

            result = requests.post(repo_url,json=repo,auth=cfg.auth_tuple)



    def create_final_capstone(self,team_name):
        cfg = self._config
        name = "Final Capstone Team {}".format(team_name)
        repo_name = name.replace(" ","_").lower()
        repo_url = "{}{}-final-capstone-team-{}".format(
            cfg.api_url_repo,
            cfg.language,
            repo_name)

        repo = {
            "name" : name,
            "is_private":True,
            "scm":"git",
            "language" : cfg.language,
            "project":{"key":cfg.project}
                    }
        result = requests.post(repo_url,json=repo,auth=cfg.auth_tuple)
